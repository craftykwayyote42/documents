
# Naming Conventions In PHP

## Class Name (CapitalCase)

Bad

```
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class articleController extends Controller {

  // Do Something

}
```

Good

```
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArticleController extends Controller {

  // Do Something

}
```

## Function Name/ Method Name / Property Name (camelCase)

```
<?php
class Animal
{
  private $name;

  public function __construct($name)
  {
    $this->name = $name;
  }

  public function sayName() {
    return "Hello, my name is $this->name";
  }

  static function info()
  {
    return "Animal class";
  }
}
$dog = new Animal("Bobby");
echo $dog->sayName(); // Hello, my name is Bobby
```

```
getName() // Right
namesList() // Wrong
listNames() // Right
sendEmail() // Right
deleteUserById($id) // Right
connectToDatabase() // Right
databaseConnection() // Wrong
prepareOutput() // Right
```

## Variables (snake_case)

```
$first_name = “John”; // Right
$last_name = “Doe”; // Right

$n1 = “John”; // Wrong  
$n2 = “Doe”; // Wrong

$my_name = “John Doe”; // Wrong  
$my_var = “John Doe”; // Wrong
```

Bad
```
$i = “Apple iPhone 6S (32GB, Rose Gold)”;
$p = “749.00”;
$t = “749.00”;
```

Good
```
$item = “Apple iPhone 6S (32GB, Rose Gold)”;
$price = “749.00”;
$total = “749.00”;

```

# Naming Conventions In Javascript

### Use meaningful and pronounceable variable names

Bad
```
const yyyymmdstr = moment().format("YYYY/MM/DD");

```

Good
```
const currentDate = moment().format("YYYY/MM/DD");
```

### Use the same vocabulary for the same type of variable

Bad
```
getUserInfo();
getClientData();
getCustomerRecord();
```

getCustomerRecord
```
getUser();
```

### Use searchable names

Bad
```
// What the heck is 86400000 for?
setTimeout(blastOff, 86400000);
```

Good
```
// Declare them as capitalized named constants.
const MILLISECONDS_IN_A_DAY = 86_400_000;

setTimeout(blastOff, MILLISECONDS_IN_A_DAY);
```

### Don't add unneeded context

Bad
```
const Car = {
  carMake: "Honda",
  carModel: "Accord",
  carColor: "Blue"
};

function paintCar(car) {
  car.carColor = "Red";
}
```

Good
```
const Car = {
  make: "Honda",
  model: "Accord",
  color: "Blue"
};

function paintCar(car) {
  car.color = "Red";
}
```

### Function arguments (2 or fewer ideally)

Bad
```
function createMenu(title, body, buttonText, cancellable) {
  // ...
}

createMenu("Foo", "Bar", "Baz", true);
```

Good
```
function createMenu({ title, body, buttonText, cancellable }) {
  // ...
}

createMenu({
  title: "Foo",
  body: "Bar",
  buttonText: "Baz",
  cancellable: true
});
```
### Remove duplicate code

Bad
```
function showDeveloperList(developers) {
  developers.forEach(developer => {
    const expectedSalary = developer.calculateExpectedSalary();
    const experience = developer.getExperience();
    const githubLink = developer.getGithubLink();
    const data = {
      expectedSalary,
      experience,
      githubLink
    };

    render(data);
  });
}

function showManagerList(managers) {
  managers.forEach(manager => {
    const expectedSalary = manager.calculateExpectedSalary();
    const experience = manager.getExperience();
    const portfolio = manager.getMBAProjects();
    const data = {
      expectedSalary,
      experience,
      portfolio
    };

    render(data);
  });
}
```

Good
```
function showEmployeeList(employees) {
  employees.forEach(employee => {
    const expectedSalary = employee.calculateExpectedSalary();
    const experience = employee.getExperience();

    const data = {
      expectedSalary,
      experience
    };

    switch (employee.type) {
      case "manager":
        data.portfolio = employee.getMBAProjects();
        break;
      case "developer":
        data.githubLink = employee.getGithubLink();
        break;
    }

    render(data);
  });
}
```
